\documentclass[a0paper]{baposter}

\usepackage{float}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{multicol}
\usepackage{wallpaper}
%% \ULCornerWallPaper{0.0005}{sheffield-logo}
%% \LLCornerWallPaper{}{bgv207.png}
%% \LLCornerWallPaper{0.14}{sunpy_powered_caps.png}
%% \URCornerWallPaper{0.0645}{mugshot}
\definecolor{blue}{rgb}{0.25, 0.25, 1.0}
\definecolor{palecyan}{rgb}{0.8, 1.0, 1.0}
\definecolor{paleblue}{rgb}{0.4, 0.7, 1.0}

\begin{document}

\begin{poster}{
  grid=false,
  % Column spacing
  colspacing=0.7em,
  % Color style
  headerColorOne=cyan,%cyan!20!white!90!black,
  headerColorTwo=blue,
  borderColor=white,%!30!white!90!black,
  boxColorOne=palecyan,%!90!white,
  boxColorTwo=paleblue,%cyan!10!white,
  % Format of textbox
  textborder=roundedleft,
  boxshade=shadeTB,
  % Format of text header
  headerborder=open,
  headershape=roundedright,
  headershade=shadeLR,
  background=none,
  bgColorOne=cyan!90!white,
  bgColorTwo=blue,%0!white,
  headerheight=0.12\textheight,
  columns=3}
  {}
  {
    Flux surface displacement in MHD simulations
  }
  {
    \\[1em]Drew Leonard - University of Sheffield\\[1em]
    {\texttt{a.j.leonard@sheffield.ac.uk}}
  }
  {}%{\includegraphics[width=0.25\linewidth]{bgv205}}

  \small
  \headerbox{Introduction}{name=intro,column=0}{
    We conduct 3D numerical simulations of a flux tube embedded in a stratified lower solar atmosphere.
    Waves are excited by introducing driving motions in the photosphere which are based on observable photospheric Alfv\'en vortices as described by \cite{onishchenko_large-scale_2015}.
    The Sheffield Advanced Code (SAC; \cite{shelyag_magnetohydrodynamic_2008}) is used to simulate the resulting motions in the flux tube.
    In the results of these simulations we inspect the flux tube's displacement from its original position in order to observe MHD wave modes.
  }

  \headerbox{Initial conditions and drivers}{name=method,column=1,span=2}{
    \begin{multicols}{2}
      \qquad \quad \includegraphics[width=0.74\columnwidth]{Figs/initdensity}
      
      \captionof{figure}{
        \begin{footnotesize}
          \emph{Left}: Background density on a slice through the simulation domain.
          Overplotted streamlines indicate magnetic field lines and show the shape of the flux tube.
          We use this background atmosphere for each of three simulations in which we implement Alv\'en vortex drivers with azimuthal wavenumber $m = -1, 0, 1$.
          The velocity fields used for these drivers are plotted below from left to right, respectively.
        \end{footnotesize}
      }

    \end{multicols}
  }

  \headerbox{Flux tube radial displacement}{name=res,column=0,below=intro,span=3}{
    In our simulations, flux surfaces are distorted by the motions caused by the driver, and we can identify radial displacement towards or away from the flux tube axis by comparing a surface's position at a given time to its original position.
    This lets us identify different wave modes in the flux tube.
    The displacements of surfaces at different distances from the axis are plotted below for each simulation, under the corresponding driver.

    \captionof{figure}{
      \begin{footnotesize}
        Horizontal velocity field for each of the photospheric drivers, $m = -1, 0, 1$ (left to right).
      \end{footnotesize}
    }
    \begin{multicols}{3}
      \includegraphics[width=\columnwidth]{Figs/driver-1}
      \includegraphics[width=\columnwidth]{Figs/driver0}
      \includegraphics[width=\columnwidth]{Figs/driver1}
    \end{multicols}
    \captionof{figure}{
      \begin{footnotesize}
        \emph{Top}: Radial displacement of flux surfaces at snapshot $150$ in the simulations.
        Arrows show the velocity field.
        \emph{Bottom}: Displacement of many flux surfaces at three fixed heights in the domain for each driver.
        Red and blue indicate movement away from and towards the flux tube axis, respectively.
      \end{footnotesize}
    }
    \begin{multicols}{3}
      \includegraphics[width=\columnwidth]{Figs/surface_dr-slice_m-1_t150}
      \includegraphics[width=\columnwidth]{Figs/surface_dr-slice_m0_t150}
      \includegraphics[width=\columnwidth]{Figs/surface_dr-slice_m1_t150}
    \end{multicols}
    \begin{multicols}{9}
      \includegraphics[width=\columnwidth]{Figs/dr-hslice_m-1_h15}
      \includegraphics[width=\columnwidth]{Figs/dr-hslice_m-1_h65}
      \includegraphics[width=\columnwidth]{Figs/dr-hslice_m-1_h115}
      \includegraphics[width=\columnwidth]{Figs/dr-hslice_m0_h15}
      \includegraphics[width=\columnwidth]{Figs/dr-hslice_m0_h65}
      \includegraphics[width=\columnwidth]{Figs/dr-hslice_m0_h115}
      \includegraphics[width=\columnwidth]{Figs/dr-hslice_m1_h15}
      \includegraphics[width=\columnwidth]{Figs/dr-hslice_m1_h65}
      \includegraphics[width=\columnwidth]{Figs/dr-hslice_m1_h115}
    \end{multicols}

    The displacement analysis shows that the $m = 0$ and $m = 1$ drivers cause kink waves (towards the axis on one side and away on the other) low in the domain near the axis, and sausage waves at the top of the domain (towards or away from the axis at all angles).
    The $m = -1$ driver leads to fluting modes at the base and kink waves elsewhere.
    It also produces greater velocities than the other drivers.
  }

  \headerbox{Acknowledgements and references}{name=refs,column=0,span=3,below=res}{
    \begin{footnotesize}
      This research made use of Astropy, a community-developed core Python package for Astronomy \cite{the_astropy_collaboration_astropy:_2013}.
      The authors also thank the yt project \cite{turk_yt:_2011} and the developers of the numpy and matplotlib Python libraries.
      \renewcommand{\section}[2]{}
      \bibliographystyle{ieeetr}
      \bibliography{refs}
    \end{footnotesize}
  }

\end{poster}
\end{document}
